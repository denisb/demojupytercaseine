# JupyterCaseine

Démonstration Publique d'une utilisation de Jupyter avec Caseine (Moodle/Vpl) :
* tri (url de l'activité vpl sur caseine [pour trouver l'id et le token nécessaire à la suite] : https://moodle.caseine.org/mod/vpl/view.php?id=47104 )
  * avec myBinder : [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fdenisb%2Fdemojupytercaseine/HEAD)
  * avec gricad (sous vpn uga pour ceux qui peuvent) : [![Binder](https://binderhub.univ-grenoble-alpes.fr/badge_logo.svg)](https://binderhub.univ-grenoble-alpes.fr/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fdenisb%2Fdemojupytercaseine/HEAD)

Remarques : 
* sous réserve d'avoir une structure similaire pour les fichiers sur caseine (3 fichiers de code, 1 fichier pour le notebook), l'exemple peut fonctionner pour un autre vpl (sinon, on peut adapter ; reste à déterminer l'intérêt de cet assemblage technologique : au niveau pédagogique Jupyter et Caseine ont des objectifs différents, pas nécessairement complémentaires s'ils ne sont pas compatibles pédagogiquement même s'ils sont compatibles d'un point de vue technologique)
* mais il y a des soucis
  * pour établir un lien avec moodle **il faut** mettre un token personnel (privé)
  * pour que le dépot soit utilisable par binder **il faut** que le dépôt soit public
  * **donc** cela entraine que l'on doit mettre un token privé en public !?!
* autres :
  * si l'on doit sauvegarder un peu/beaucoup de chose sur git, alors pourquoi ne pas tout sauver sur git et se servir de git comme dépot de fichier plutot que moodle (il y a une version de vpl qui fonctionne avec des dépots git) ?
  * si l'utilisation personnelle via jupyterhub ou sa machine n'est pas si complexe (juste copie au départ et à la fin de/vers moodle), pourquoi faire plus compliqué ?

